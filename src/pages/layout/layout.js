import './layout.scss'
import { Grid, Box } from '@mui/material'

const layout = () => {
    return (
        <>
            <Grid container spacing={2}>
                <Grid item xs md={2} className="left__content">
                    lala
                </Grid>
                <Grid item xs={12} md={9} className="right__content">
                    <Box sx={{
                        height: '100px',
                        // width: '90%', 
                        p: '10px',
                        marginTop: '20px',
                        borderRadius: '4px',
                        boxShadow: 'rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px;'
                    }}>
                        <h1 className="right__content__title">Список производств в секторе</h1>
                        <Box>
                            test
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </>
    )
}

export default layout